CC = gcc
CFLAGS = -Iinclude/tool

main: main.c src/function.c
	$(CC) $(CFLAGS) -o main main.c src/function.c

clean:
	rm -f main