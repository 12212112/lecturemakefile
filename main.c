#include <stdio.h>
#include "function.h"

int main() {
    int num1 = 10;
    int num2 = 20;
    
    int result = compare(num1, num2);
    
    if (result == 1) {
        printf("%d is greater than %d\n", num1, num2);
    } else if (result == -1) {
        printf("%d is less than %d\n", num1, num2);
    } else {
        printf("%d is equal to %d\n", num1, num2);
    }
    
    return 0;
}
